package test.test.xmlresponse.model;

import lombok.*;

import javax.xml.bind.annotation.*;

@Data
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "response")
@XmlAccessorType(XmlAccessType.NONE)
public class Response {

    @XmlAttribute
    private Long id;

    @XmlAttribute
    private String dts;

    @XmlElement
    private Long p_id;

    @XmlElement
    private int status;

    @XmlElement
    private String message;
}
