package test.test.xmlresponse.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import test.test.xmlresponse.entities.BaseEntity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Request extends BaseEntity {

    @NotNull(message = "Value must not be null")
    @Positive(message = "Value must be more than 0")
    private Long supplier_id;

    @NotNull(message = "Value must not be null")
    private String account;

    @NotNull(message = "Value must not be null")
    @Positive(message = "Value must be more than 0")
    private double amount;

    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "Value must not be null")
    private LocalDateTime date;

    @NotNull(message = "Value must not be null")
    private String command;
}
