package test.test.xmlresponse.model;

import lombok.*;

import javax.validation.Valid;


@Data
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class RequestDto {

    @Valid
    private Request request;
}
