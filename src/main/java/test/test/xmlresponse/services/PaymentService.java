package test.test.xmlresponse.services;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import test.test.xmlresponse.entities.Payment;
import test.test.xmlresponse.entities.Supplier;
import test.test.xmlresponse.model.Request;
import test.test.xmlresponse.model.Response;
import test.test.xmlresponse.repositories.PaymentRepository;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PaymentService {
    private final PaymentRepository paymentRepository;
    private final SupplierService supplierService;

    public Response handleCheckRequest(Request request) {
        Optional<Payment> optionalPayment = this.paymentRepository.findByRequest_id(request.getId());
        if (optionalPayment.isPresent()){
            return Response.builder()
                    .id(request.getId())
                    .dts(request.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                    .status(0)
                    .p_id(optionalPayment.get().getId())
                    .message("ACCOUNT EXISTS")
                    .build();
        }

        return Response.builder()
                .id(request.getId())
                .dts(request.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                .status(2)
                .p_id(0L)
                .message("ACCOUNT DOESN'T EXIST")
                .build();
    }

    public Response handlePayRequest(Request request, Supplier supplier){
        Payment payment = Payment.builder()
                .account(request.getAccount())
                .amount(request.getAmount())
                .date(request.getDate())
                .request_id(request.getId())
                .supplier(supplier)
                .build();

        return Response.builder()
                .id(request.getId())
                .dts(request.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                .status(1)
                .p_id(this.paymentRepository.save(payment).getId())
                .message("PAYMENT CONFIRMED")
                .build();
    }

    public Optional<Supplier> getSupplierById(Long id){
        return this.supplierService.getSupplierById(id);
    }

    public Response createErrorResponse(Request request, String message) {
        return Response.builder()
                .id(request.getId())
                .status(2)
                .dts(request.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                .message(message)
                .build();
    }

    public String getListOfErrors(List<FieldError> fieldErrors) {
        String message = "";
        for (FieldError error : fieldErrors) {
            message = message + "\n\t\t" +  error.getField() + " : " + error.getDefaultMessage() + "\t(rejected: " + error.getRejectedValue() + ")";
        }
        return message;
    }
}
