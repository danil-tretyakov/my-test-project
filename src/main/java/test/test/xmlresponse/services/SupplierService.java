package test.test.xmlresponse.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import test.test.xmlresponse.entities.Supplier;
import test.test.xmlresponse.repositories.SupplierRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SupplierService {
    private final SupplierRepository supplierRepository;

    public Optional<Supplier> getSupplierById(Long supplierId){
        return this.supplierRepository.findById(supplierId);
    }
}
