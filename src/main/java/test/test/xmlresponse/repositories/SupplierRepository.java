package test.test.xmlresponse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import test.test.xmlresponse.entities.Supplier;

public interface SupplierRepository extends JpaRepository<Supplier, Long> {
}
