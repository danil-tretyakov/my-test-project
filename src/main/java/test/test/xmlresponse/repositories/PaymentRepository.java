package test.test.xmlresponse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import test.test.xmlresponse.entities.Payment;

import java.util.Optional;

public interface PaymentRepository extends JpaRepository<Payment, Long> {
    
    @Query("select p from Payment p where p.request_id = ?1")
    Optional<Payment> findByRequest_id(Long id);
}
