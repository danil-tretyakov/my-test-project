package test.test.xmlresponse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import test.test.xmlresponse.entities.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
}
