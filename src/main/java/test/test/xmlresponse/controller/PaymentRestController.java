package test.test.xmlresponse.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import test.test.xmlresponse.entities.Supplier;
import test.test.xmlresponse.exceptions.SupplierNotFoundException;
import test.test.xmlresponse.model.Request;
import test.test.xmlresponse.model.RequestDto;
import test.test.xmlresponse.model.Response;
import test.test.xmlresponse.services.PaymentService;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class PaymentRestController {

    private final PaymentService paymentService;

    @PostMapping(value = "/check", consumes = "application/json", produces = "application/xml")
    public ResponseEntity<Response> checkIfAccountExists(@Valid @RequestBody RequestDto requestDto, BindingResult bindingResult){
        if (bindingResult.hasFieldErrors()){
            return new ResponseEntity<>(this.paymentService.createErrorResponse(requestDto.getRequest(), this.paymentService.getListOfErrors(bindingResult.getFieldErrors())), HttpStatus.BAD_REQUEST);
        }

        if (!requestDto.getRequest().getCommand().equals("check")){
            return new ResponseEntity<>(this.paymentService.createErrorResponse(requestDto.getRequest(), "WRONG COMMAND"), HttpStatus.BAD_REQUEST);
        }
        Response response = this.paymentService.handleCheckRequest(requestDto.getRequest());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(value = "/pay", consumes = "application/json", produces = "application/xml")
    public ResponseEntity<Response> createNewPayment(@Valid @RequestBody RequestDto requestDto, BindingResult bindingResult){
        if (bindingResult.hasFieldErrors()){
            return new ResponseEntity<>(this.paymentService.createErrorResponse(requestDto.getRequest(), this.paymentService.getListOfErrors(bindingResult.getFieldErrors())), HttpStatus.BAD_REQUEST);
        }
        Request request = requestDto.getRequest();
        if (!request.getCommand().equals("pay")){
            return new ResponseEntity<>(this.paymentService.createErrorResponse(request, "WRONG COMMAND"), HttpStatus.BAD_REQUEST);
        }
        Optional<Supplier> supplier = this.paymentService.getSupplierById(request.getSupplier_id());
        if (supplier.isEmpty()){
            return new ResponseEntity<>(this.paymentService.createErrorResponse(request, new SupplierNotFoundException("SUPPLIER NOT FOUND").getMessage()),
                    HttpStatus.NOT_FOUND);
        }
        Response response = this.paymentService.handlePayRequest(request, supplier.get());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
