package test.test.xmlresponse.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "payments")
public class Payment extends BaseEntity {

    @Column
    private String account;

    @Column
    private double amount;

    @Column
    private LocalDateTime date;

    @ManyToOne(fetch = FetchType.LAZY)
    private Supplier supplier;

    @Column
    private Long request_id;

}
